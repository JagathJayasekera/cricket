#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"cricket.h"
#include"config.h" //auto generated

Cricket *make_cricket(unsigned id){
  Cricket *cri;
  if((cri=(Cricket *)malloc(sizeof(Cricket)))==NULL){
    fprintf(stderr,"failed to allocate Cricket structure\n");
    exit(EXIT_FAILURE);
  }
  cri->active = 0;
  cri->id = id;
  cri->name = NULL;

  return cri;
}

void make_cricket_active(Cricket *cri){
  cri->active=1;
}
int is_cricket_active(Cricket *cri){
  return cri->active;
}
void free_cricket(Cricket *cri){
  free(cri->name);  
  free(cri);
}
void set_cricket_name(Cricket *cri, char *name){
  cri->name = name;
}

int serialize_cricket(char *buffer, Cricket *stud)
{
  size_t offset = 0;

  memcpy(buffer, &cri->id, sizeof(cri->id));
  offset = sizeof(cri->id);
  memcpy(buffer+offset, &cri->active, sizeof(cri->active));
  offset = offset + sizeof(cri->active);
  memcpy(buffer+offset, cri->name, strlen(cri->name)+1);
  offset = offset + strlen(cri->name)+1;

  return offset;
}

int deserialize_student(char *buffer, Cricket *stud)
{
  size_t offset = 0;

  memcpy(&cri->id, buffer, sizeof(cri->id));
  offset = sizeof(cri->id);
  memcpy(&cri->active, buffer+offset, sizeof(cri->active));
  offset = offset + sizeof(cri->active);
  memcpy(cri->name, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;

  return offset;
}
void print_cricket(Cricket *cri)
{
  printf("Cricket id:%d\n", cri->id);
  printf("Cricket name:%s\n", cri->name);
}

Cricket *alloc_blank_cricket()
{

  Cricket *cri;

  if ((cri = (Cricket *)malloc(sizeof(Cricket))) == NULL) {
    fprintf(stderr, "Failed to allocate Cricket structure!\n");
    exit(EXIT_FAILURE);
  }
  cri->active = 0;
  cri->id = 0;
  if ((cri->name = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }

  return cri;
}

