#include<stdio.h> /*standard input output header file*/
#include<stdlib.h> /*defines memory allocation,process control funtions*/
typedef struct{  /*gives user defined type names*/
  char *name;
  unsigned id;
  int active;
} Cricket;

Cricket *make_cricket(unsigned id); /*calls function make_cricket*/
void free_cricket(Cricket *cri);
void make_cricket_active(Cricket *cri);
void set_cricket_name(Cricket *cri, char *name);
int is_cricket_active(Cricket *cri);
int serialize_cricket(char *buffer, Cricket *cri);
int deserialize_cricket(char *buffer, Cricket *cri);
void print_cricket(Cricket *cri);
Cricket *alloc_blank_student();
