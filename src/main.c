#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include<stdio.h>
#include<stdlib.h>

#include "cricket.h"
#include "config.h" // auto generated

int main()
{
  Cricket *cri1;
  int       sock;
  struct    sockaddr_in servaddr;
  char      buffer[MAX_BUFFER];
  ssize_t bytes_encoded, bytes_sent;
  
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT);
  
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
  
  cri1=make_cricket(2013);
  cricket_name(cri1, "mark taylor");
  make_cricket_active(cri1);
  
  bytes_encoded = serialize_cricket(buffer, cri1);
  printf("bytes_encoded = %d\n", (int)bytes_encoded);

  bytes_sent = send(sock, buffer, bytes_encoded, 0);
  
  if (bytes_sent != bytes_encoded) {
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }
  
  if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }
  
  free_cricket(cri1);
  return EXIT_SUCCESS;
  
} 
